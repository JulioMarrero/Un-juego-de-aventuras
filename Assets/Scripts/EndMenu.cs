using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndMenu : MonoBehaviour {
    public Text endMenuText;
    public Button restartButton;
    public Button quitToMainMenuButton;
    public GameObject stars0;
    public GameObject stars1;
    public GameObject stars2;
    public GameObject stars3;
    

    private void Start() {
        gameObject.SetActive(false);
        restartButton.onClick.AddListener(RestartGame);
        quitToMainMenuButton.onClick.AddListener(QuitToMainMenu);
    }

    public void ToggleEndMenu(bool set) {
        gameObject.SetActive(set);
        if (!set) return;
        Stars();
        endMenuText.text = InsultsRecord.IsWin()
            ? "Enhorabuena!\nHas ganado el juego!"
            : "Has perdido.\nPrueba a jugar de nuevo.";
    }

    private void Stars() {

        stars0.SetActive(false);
        stars1.SetActive(false);
        stars2.SetActive(false);
        stars3.SetActive(false);
        switch (InsultsRecord.Win) {
            case 1:
                stars1.SetActive(true);
                break;
            case 2:
                stars2.SetActive(true);
                break;
            case 3:
                stars3.SetActive(true);
                break;
            default:
                stars0.SetActive(true);
                break;
        }
    }

    private void RestartGame() {
        ToggleEndMenu(false);
        InsultsRecord.ResetStats();
        SceneManager.LoadScene("Game");
        Debug.Log("Reiniciando partida...");
    }

    private void QuitToMainMenu() {
        ToggleEndMenu(false);
        InsultsRecord.ResetStats();
        SceneManager.LoadScene("MainMenu");
        Debug.Log("Accediendo al menú principal...");
    }
}
