﻿# Un juego de Aventuras

### PEC1

Siguiendo las directrices encomendadas para esta práctica, he creado este juego, muy simple en su funcionamiento.
El juego incluye sonidos, gráficos y una cómoda jugabilidad.

### Funcionamiento

El objetivo del juego es ganar en una lucha de insultos al ordenador al mejor de 3 rondas. En cada ronda,
el ordenador dirá un insulto al azar y el jugador tiene que responder de entre todas las frases la adecuada.
Si escoje la frase adecuada, el jugador suma un punto que se añade al marcador. Si responde con otra frase, 
será el ordenador el que sume un punto a su marcador. Y de esta forma, el que consiga más puntos gana.

Entre insulto e insulto, los personajes se acercan al medio y pelean entre si en un duelo de espadas. Luego 
vuelven a sus puestos para seguir su duelo hablado. Mientras los personajes estén luchando, el jugador 
podrá mirar el resto de respuestas, pero no podrá presionar ninguno de los botones.

Una vez decidido el ganador del duelo, saldrá una pantalla indicando el final del juego con el resultado obtenido:
Victoria o Derrota. Aún así el jugador podrá conseguir de 0 a 3 estrellas dependiendo de lo bien que lo haya hecho
durante el juego. El jugador, después, podrá decidir si jugar otra partida o si de lo contario salir al menú principal
del cual podrá empezar un juego nuevo, o salir del juego.